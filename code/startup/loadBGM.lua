-- Load background

function loadB(file)
    tmp = love.audio.newSource("bgm/"..file..".ogg", "stream")
    tmp:setLooping(true)
    return tmp
end

bgm = {
    menu = loadB("menu"),
    cave = loadB("cave"),
    aggressive = loadB("aggressive"),
}

function stopBGM()
    bgm.menu:stop()
    bgm.cave:stop()
    bgm.aggressive:stop()
end

function playBGM(track)
    stopBGM()
    track:play()
end
