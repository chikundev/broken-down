-- Returns the sign of a number
function math.sign(val)
    returnValue = 0
    if val < 0 then returnValue = -1
    elseif val > 0 then returnValue = 1 end
    return returnValue
end

-- Returns the rounded value of a number
function math.round(val)
    return math.floor(val + 0.5)
end

-- Checks if two rectangle objects overlap
function math.overlap(obj1, obj2)
    return (obj1.x < obj2.x + obj2.width and
        obj2.x < obj1.x + obj1.width and
        obj1.y < obj2.y + obj2.height and
        obj2.y < obj1.y + obj1.height)
end
