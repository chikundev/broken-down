-- Create the player object

player = {
    x = 0,
    y = 0,
}
state ={
    baby = {
        width = 26,
        height = 58,
        stepMulti = 1,
    },
    dad = {
        width = 58,
        height = 109,
        stepMulti = 3,
    }
}

function player.setup(px, py)
    if px ~= nil then
        player.x = px
        player.y = py
    end
    player.state = "baby"
    player.width = state[player.state].width
    player.height = state[player.state].height
    player.facing = 1
    player.health = 100
    player.dead = false
    player.canJump = false
    player.canSwap = false
    player.jumpHold = -1
    player.xSpeed = 0
    player.ySpeed = 0
    player.yMax = -1
    player.accel = 1600
    player.xMaxSpeed = 380
    player.image = gfx.baby.still[1]
    player.imageStep = 1
    player.imageOver = nil -- Override current image setting
    player.isImmune = false
    player.immuneTimer = 0
    player.transition = 0
    player.transmod = 1
    portalCount = 0
end

function player.draw(vx, vy)
    totem = totems.baby
    if player.state == "baby" then totem = totems.dad end
    -- Draw player
    g.setColor(255, 255, 255)
    px = vx
    if player.facing == -1 then px = vx - player.width end
    g.draw(player.image, math.round(player.x - px), math.round(player.y - vy + (player.height - player.image:getHeight())), 0, player.facing, 1)

    -- Warp indicator
    warpDraw = false
    for k,v in ipairs(arches) do
        d = true
        if v.exclusive ~= nil then
            if v.exclusive ~= player.state then
                dd = false
            end
        end
        if math.overlap(v, player) and v.next ~= nil and dd then
            warpDraw = true
        end
    end
    if portalCount == 2 then warpDraw = true end
    ak = false ; ax = false
    akv = "" ; axv = ""
    for k,v in ipairs(pWarps) do
        if math.overlap(v, player) then
            ak = true
            akv = v
        elseif math.overlap(v, totem) then
            ax = true
            axv = v
        end
    end
    if (ak and ax) and (akv.name == axv.link) and (axv.name == akv.link) then warpDraw = true end
    if warpDraw then
        g.draw(gfx.up, player.x + player.width / 2 - 12 - vx, player.y - 32 - vy)
    end

    -- Draw portal indicators
    portalCount = 0
    if math.overlap(totem, portal) then
        g.setColor(32, 96, 192, 224)
        portalCount = 1
    else
        g.setColor(32, 32, 32, 192)
    end
    g.circle("fill", 830, 24, 12)
    if math.overlap(player, portal) then
        g.setColor(32, 96, 192, 224)
        portalCount = portalCount + 1
    else
        g.setColor(32, 32, 32, 192)
    end
    g.circle("fill", 794, 24, 12)
end

function player.update(dt)
    if player.transition == 0 then
        if player.dead then
            if count == nil then
                sta = "baby"
                if player.state == "baby" then
                    sta = "dad"
                end
                ran = math.ceil(math.random(3))
                if sta == "baby" then
                    if ran == 1 then
                        tex = "Kid: DAAAAAD!"
                    elseif ran == 2 then
                        tex = "Kid: NOOOOOO!"
                    else
                        tex = "Kid: Dad? Daaaad?"
                    end
                else
                    if ran == 1 then
                        tex = "Dad: Not again!"
                    elseif ran ==2 then
                        tex = "Dad: NOOOOOO!"
                    else
                        tex = "Dad: AAAAAAH!"
                    end
                end
                count = 0
                sfx[sta].deathReact[ran]:play()
                sfx.die:play()
                addSub(tex, 1.2)
            end
            count = count + dt
            if count > 1 then
                map.load(levelName)
                count = nil
            end
        else
            if (k.isDown("r") or plsSwap) and player.canSwap and player.canJump and not player.dead then
                player.transform()
                player.canSwap = false
            end
            if not (k.isDown("r") or plsSwap) then
                player.canSwap = true
            end
            player.vertical(dt)
            player.horizontal(dt)
            player.animate(dt)
        end
    else
        player.transition = player.transition - 2 * dt
        if player.transition < 0 then
            player.transition = 0
        end
    end
end

function player.overlaps()
    return map.touches(player)
end

function player.horizontal(dt)
    -- Calculate button movement
    local currentForce = 0
    if k.isDown("left") or moveLeft then
        currentForce = -1 ; player.facing = -1
    elseif k.isDown("right") or moveRight then
        currentForce = 1  ; player.facing = 1
    end
    sSign = math.sign(player.xSpeed + currentForce)
    if currentForce == 0 or sSign ~= currentForce then
        -- SLOW DOWN
        player.xSpeed = player.xSpeed - (sSign * player.accel * dt)
        if sSign ~= math.sign(player.xSpeed) then
            player.xSpeed = 0
        end
    else
        -- SPEED UP
        player.xSpeed = player.xSpeed + (currentForce * player.accel * dt)
        if math.abs(player.xSpeed) > player.xMaxSpeed then
            player.xSpeed = player.xMaxSpeed * math.sign(player.xSpeed)
        end
    end

    -- Move player horiztonally
    player.x = player.x + player.xSpeed * dt

    if player.overlaps() then
        while player.overlaps() do
            ss = math.sign(player.xSpeed)
            if ss == 0 then ss = 1 end
            player.x = math.round(player.x) - ss
        end
        player.xSpeed = 0
    end

    for k,b in ipairs(death) do
        if math.overlap(player, b) then
            player.health = player.health - dt * 750
        end
    end

    if player.x < 0 then player.x = 0 ; player.xSpeed = 0
    elseif player.x + player.width > map.getWidth() then player.x = map.getWidth() - player.width ; player.xSpeed = 0 end

    if player.health <= 0 then player.dead = true end
end

function player.vertical(dt)
    player.y = player.y + player.ySpeed * dt
    player.ySpeed = player.ySpeed + 1800 * dt
    sSign = math.sign(player.ySpeed)
    if player.overlaps() then
        if sSign == 0 then ss = 1 else ss = sSign end
        while player.overlaps() do
            player.y = math.ceil(player.y) - ss
        end
        player.ySpeed = 0
        if sSign == 1 then player.canJump = true ; player.jumpHold = 0 ; player.yMax = player.y
            if player.state == "dad" then player.jumpHold = 0 ; player.yMax = player.y-96 end
        else player.canJump = false ; player.jumpHold = -1 end
    else
        player.canJump = false
    end

    if k.isDown(" ") or plsJump then
        if player.canJump or (player.y > player.yMax and player.jumpHold == 0 and player.ySpeed <= 0) then
            player.ySpeed = -500
            if player.canJump then
                ss = sfx[player.state].jump
                ss:setPitch(0.8 + math.random() * 0.4)
                ss:play()
            end
        end
    else
        player.jumpHold = -1
    end
    if player.y + player.height > map.getHeight() then player.dead = true end
end

function player.animate(dt)
    array = { }
    multi = 1 * state[player.state].stepMulti
    if math.abs(player.xSpeed) < 100 then
        multi = 1.5
        array = gfx[player.state].still
    else
        multi = (math.abs(player.xSpeed) / player.xMaxSpeed * 4)
        array = gfx[player.state].walk
    end
    while player.imageStep > #array + 1 do player.imageStep = player.imageStep - 1 end
    player.imageStep = player.imageStep + dt * multi
    if player.imageStep >= #array + 1 then
        player.imageStep = player.imageStep - #array
    end
    player.image = array[math.floor(player.imageStep)]

end

function player.transform()
    sfx.swap:play()
    returnValue = false
    player.transition = 1
    player.transmod = 1
    wid = player.width - totem.width
    hei = player.height - totem.height
    if player.state == "baby" then
        totems.baby.x = player.x
        totems.baby.y = player.y-4
        totems.baby.facing = player.facing
        player.state = "dad"
        player.x = totems.dad.x
        player.y = totems.dad.y+1
        player.facing = totems.dad.facing
    else
        totems.dad.x = player.x
        totems.dad.y = player.y-1
        totems.dad.facing = player.facing
        player.state = "baby"
        player.x = totems.baby.x
        player.y = totems.baby.y +4
        player.facing = totems.baby.facing
    end
    player.width = state[player.state].width
    player.height = state[player.state].height
    return returnValue
end
