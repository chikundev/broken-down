-- Load sound effects

function loadS(file)
    return love.audio.newSource("sfx/" .. file .. ".ogg", "static")
end

sfx = {
    baby = {
        jump = loadS("baby/jump"),
        flyingDad = loadS("baby/flyingDad"),
        okay = loadS("baby/okay"),
        deathReact = {
            loadS("baby/death1"),
            loadS("baby/death2"),
            loadS("baby/death3"),
        },
        funnyGround = loadS("baby/funnyGround"),
        theExit = loadS("baby/theExit"),
        weCanGetOut = loadS("baby/weCanGetOut"),
        wellGosh = loadS("baby/wellGosh"),
        workTogether = loadS("baby/workTogether")
    },
    dad = {
        jump = loadS("dad/jump"),
        babyZefronNo = loadS("dad/babyZefronNo"),
        engine = loadS("dad/engine"),
        noGood = loadS("dad/noGood"),
        notAgain = loadS("dad/notAgain1"),
        deathReact = {
            loadS("dad/notAgain2"),
            loadS("dad/noooo"),
            loadS("dad/aaaah")
        },
        almostThere = loadS("dad/almostThere"),
        noooo = loadS("dad/noooo"),
        warps = loadS("dad/warps"),
        weCanDoThis = loadS("dad/weCanDoThis"),
    },
    pause = loadS("pause"),
    resume = loadS("resume"),
    exit = loadS("close"),
    car = loadS("car"),
    crash = loadS("crash"),
    wrong = loadS("wrong"),
    complete = loadS("levelComplete"),
    ground = loadS("ground"),
    switch = loadS("switch"),
    die = loadS("die"),
    pipe = loadS("pipe"),
    swap = loadS("swap"),
    warp = loadS("warp"),
}
