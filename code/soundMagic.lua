-- plays sounds depending on placement

function soundStep()
if gameState == "play" then

    if levelName == "lvl/level02" then
        snd1 = {
            x = 512,
            y = 480,
            width = 64,
            height = 128
        }
        if math.overlap(player, snd1) and playedSounds.wcdt == nil then
            sfx.baby.weCanGetOut:play()
            playedSounds.wcdt = true
            addSub("Kid: C'mon Dad, we can get out of here.", 2)
        end
    end

    if levelName == "lvl/level03" then
        snd1 = {
            x = 128,
            y = 512,
            width = 64,
            height = 160
        }
        if math.overlap(player, snd1) and playedSounds.warp == nil then
            sfx.dad.warps:play()
            playedSounds.warp = true
            addSub("Dad: I guess we just go through these.", 2)
        end
    end

    if levelName == "lvl/level04" then
        snd1 = {
            x = 128,
            y = 384,
            width = 64,
            height = 192
        }
        if math.overlap(player, snd1) and playedSounds.funnyG == nil then
            sfx.baby.funnyGround:play()
            playedSounds.funnyG = true
            addSub("Kid: That's some funny ground over there, Dad.", 2)
        end
    end

    if levelName == "lvl/level05" then
        snd1 = {
            x = 5 * 32,
            y = 42 * 32,
            width = 64,
            height = 192
        }
        if math.overlap(player, snd1) and playedSounds.bbzf == nil then
            sfx.dad.weCanDoThis:play()
            playedSounds.bbzf = true
            addSub("Dad: Baby Zefron, we can do this.", 2)
        end
    end

    if levelName == "lvl/level06" then
        snd1 = {
            x = 4 * 32,
            y = 26 * 32,
            width = 64,
            height = 160
        }
        if math.overlap(player, snd1) and playedSounds.lwt == nil then
            sfx.baby.workTogether:play()
            playedSounds.lwt = true
            addSub("Kid: Let's work together, Dad.", 2)
        end
    end

    if levelName == "lvl/level07" then
        snd1 = {
            x = 5 * 32,
            y = 15 * 32,
            width = 64,
            height = 192
        }
        if math.overlap(player, snd1) and playedSounds.at == nil then
            sfx.dad.almostThere:play()
            playedSounds.at = true
            addSub("Dad: I think we're almost there.", 2)
        end
    end
end
end
