view = {
    x = 0,
    y = 0,
    width = 854,
    height = 480,
}

function view.update()
    if gameState == "play" then
        if player.transition == 0 then
            follow = player
        else
            totem = totems.baby
            if player.state == "baby" then totem = totems.dad end
            anti = player.transition
            aaa = { }
            aaa.x = player.x + (totem.x - player.x) * anti
            aaa.y = player.y + (totem.y - player.y) * anti
            aaa.width = player.width + (totem.width - player.width) * anti
            aaa.height = player.height + (totem.height - player.height) * anti
            follow = aaa
        end
    elseif gameState == "intro" then follow = fakeCar
    elseif gameState == "intro2" then follow = dadChar
    elseif gameState == "outro" then follow = tri end
    if getScale() == scaleW then
        view.height = (g.getHeight() / 480) / (g.getWidth() / 854) * 480
    else
        view.width = (g.getWidth() / 854) / (g.getHeight() / 480) * 854
    end
    view.x = follow.x + follow.width / 2 - view.width / 2
    view.y = follow.y + follow.height / 2 - view.height / 2
    if view.x < 0 then view.x = 0 end
    if view.y < 0 then view.y = 0 end
    if view.x + view.width > map.getWidth() then view.x = map.getWidth() - view.width end
    if view.y + view.height > map.getHeight() then view.y = map.getHeight() - view.height end
end
