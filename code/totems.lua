totems = { }
totems["dad"] = {
    x = 0,
    y = 0,
    width = gfx.dad.still[1]:getWidth(),
    height = gfx.dad.still[1]:getHeight(),
    facing = 1,
    image = gfx.dad.still[1]
}
totems["baby"] = {
    x = 0,
    y = 0,
    width = gfx.baby.still[1]:getWidth(),
    height = gfx.baby.still[1]:getHeight(),
    facing = 1,
    image = gfx.baby.still[1]
}

function totems.setup(vx, vy, width, height)
    totems.baby.x = vx + (width - totems.baby.width) / 2
    totems.baby.y = vy + (height - totems.baby.height)
    totems.dad.x = vx + (width - totems.dad.width) / 2
    totems.dad.y = vy + (height - totems.dad.height)
end

function totems.draw(vx, vy)
    totem = totems.baby
    if player.state == "baby" then totem = totems.dad end
    xOffset = 0
    if totem.facing < 0 then xOffset = totem.image:getWidth() end
    g.draw(totem.image, totem.x - vx + xOffset, totem.y - vy, 0, totem.facing, 1)
end
